<?php
namespace Pulsestorm\TutorialProxy1\Model;

use Pulsestorm\TutorialProxy1\Model\SlowLoading\Proxy;
class Example
{
    protected $fast;
    protected $slow;
    public function __construct(FastLoading $fast,
                                Proxy $slow)
    {
        $this->fast = $fast;
        $this->slow = $slow;
    }

    public function sayHelloWithFastObject()
    {
        $this->fast->hello();
    }

    public function sayHelloWithSlowObject()
    {
        $this->slow->hello();
    }
}
